import { Filter } from './types';
import { Contact } from './models/contact';

export const getContacts = (): Contact[] => {
    const contacts = localStorage.getItem('contacts') || '[]';
    return JSON.parse(contacts).map(
        (item: Contact) => new Contact(item.name, item.email, item.phone, item.isFavourite, item.id)
    );
};

export const setContacts = ( contacts: Contact[] ): void => {
    localStorage.setItem('contacts', JSON.stringify(contacts));
};


export const getFilter = (): Filter => {
    return localStorage.getItem('filter') as Filter || 'all';
};

export const setFilter = ( filter: Filter ): void => {
    localStorage.setItem('filter', filter)
};
