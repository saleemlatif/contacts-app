import * as React from 'react';
import { Contact, ContactInterface } from "../../models/contact";


type ContactItemProps = {
    id: number;
    name: string;
    email: string;
    phone: string;
}


const ContactItem: React.FC<ContactItemProps> = ({ id, name, email, phone }) => {
    return (
        <>
            <div>
                <label htmlFor={'name-' + id}>Name: </label>
                <span id={'name-' + id}>{ name }</span>
            </div>
            <div>
                <label htmlFor={'email-' + id}>Email: </label>
                <span id={'email-' + id}>{ email }</span>
            </div>
            <div>
                <label htmlFor={'phone-' + id}>Phone: </label>
                <span id={'phone-' + id}>{ phone || 'MISSING' }</span>
            </div>
        </>
    );
};


type ContactEditFormItemProps = {
    id: number;
    name: string;
    email: string;
    phone: string;
    onSubmit: ( contact: ContactInterface ) => void;
}



const ContactEditFormItem: React.FC<ContactEditFormItemProps> = ({ id, name, email, phone, onSubmit }) => {
    const [ contact, setContact ] = React.useState<ContactInterface>({ id, name, email, phone });

    const handleInputChange = ( event: React.BaseSyntheticEvent ): void => {
        const target = event.target;

        // Update on change.
        setContact({...contact, [target.name]: target.value});
    };


    const handleFormSubmit = ( event: React.BaseSyntheticEvent ): void => {
        onSubmit(contact);

        // Prevent normal form submission flow.
        event.preventDefault();
    };

    return (
        <form onSubmit={ handleFormSubmit }>
            <div>
                <label htmlFor={'name-' + id}>Name: </label>
                        <input
                            type='text'
                            name='name'
                            placeholder='Name'
                            required
                            value={ contact.name }
                            onChange={ handleInputChange }
                        />

            </div>
            <div>
                <label htmlFor={'email-' + id}>Email: </label>
                        <input
                            type='text'
                            name='email'
                            placeholder='Email'
                            required
                            value={ contact.email }
                            onChange={ handleInputChange }
                        />
            </div>
            <div>
                <label htmlFor={'phone-' + id}>Phone: </label>
                        <input
                            type='tel'
                            name='phone'
                            pattern='[0-9]{3}-[0-9]{4}-[0-9]{3}'
                            placeholder='Phone: 111-1111-111'
                            value={ contact.phone }
                            onChange={ handleInputChange }
                        />
            </div>
            <div className={'submit-button-wrapper'}><button className={'button'} type={'submit'}>Update</button></div>
        </form>
    );
};



type EditableContactListItemProps = {
    id: number;
    name: string;
    email: string;
    phone: string;
    isFavourite: boolean;
    onUpdate: (id: number, contact: ContactInterface ) => void;
    onDelete: (id: number ) => void;
}


export const EditableContactListItem: React.FC<EditableContactListItemProps> =
    ({
         id, name, email, phone, isFavourite, onUpdate, onDelete
    }) => {

    const [ contact, setContact ] = React.useState<ContactInterface>({id, name, email, phone, isFavourite });
    const [ mode, setMode ] = React.useState<'edit'|'view'>('view');

    const handleSubmit = ( contactItem: Contact ): void => {
        setContact({ ...contact, ...contactItem });
        setMode('view');
    };

    const toggleFavourite = (  ): void => {
        // Update on change.
        setContact({...contact, isFavourite: !contact.isFavourite});
    };

    React.useEffect(() => {
        onUpdate(id, contact);
    }, [contact]);


    return (
        <div className={'list-item flex-container ' + 'mode-' + mode}>
            <div className={'row'}>
                {
                    mode === 'view' ?
                        <ContactItem
                            id={contact.id}
                            name={contact.name}
                            phone={contact.phone}
                            email={contact.email}
                        /> :
                        <ContactEditFormItem
                            id={contact.id}
                            name={contact.name}
                            phone={contact.phone}
                            email={contact.email}
                            onSubmit={ handleSubmit }
                        />
                }
            </div>

            <div
                onClick={ toggleFavourite }
                className={'pointer row'}
            >
                { contact.isFavourite ? <>&#9733;</>: <>&#9734;</> }
            </div>

            <div className={'right row'}>
                <button
                    className={'button delete-button'}
                    onClick={(): void => onDelete(id)}
                >Delete</button>
                <button
                    onClick={ (): void => setMode(mode === 'view' ? 'edit': 'view') }
                    className={'button cancel-button'}
                >
                    { mode === 'view' ? 'Click to Edit': 'Cancel' }
                </button>
            </div>
        </div>
    );
};

