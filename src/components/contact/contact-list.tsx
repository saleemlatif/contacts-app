import * as React from 'react';

import { Contact } from '../../models/contact';
import { ContactForm } from './contact-form';
import { EditableContactListItem } from './editable-contact-list-item';
import { Filter } from '../../types';
import { getFilter, setFilter as setStoreFilter, getContacts, setContacts as setStoreContacts } from '../../store';



export const ContactList: React.FC = () => {
    const [ contacts, setContacts ] = React.useState(getContacts());
    const [ filter, setFilter ] = React.useState<Filter>(getFilter());

    const addContact = ( contact: Contact ): void => {
        setContacts([...contacts, contact])
    };

    const deleteContact = ( id: number ): void => {
        setContacts(contacts.filter(item => item.id !== id))
    };

    const updateContact = ( id: number, contact: Contact ): void => {
        const contactItem = contacts.find(item => item.id === id);
        contactItem.mergeWith(contact);

        setContacts(contacts.map(item => item.id === id ? contactItem: item))
    };


    // Save filter on local storage
    React.useEffect(() => {
        setStoreFilter(filter);
    }, [filter]);

    // Save contacts on local storage
    React.useEffect(() => {
        setStoreContacts(contacts);
    }, [contacts]);


    const filteredContacts = contacts.filter( item => filter === 'favourite' ? item.isFavourite : true);

    return (
        <>
            <header className={'header'}>Contacts</header>
            <div className={'contact-form'}>
                <ContactForm onSubmit={ addContact } />
            </div>

            <div className={'content'}>
                <div className={'filters'}>
                    <span>Show: </span>
                    <a
                        className={ filter === 'all' ? 'selected': '' }
                        href="#all"
                        onClick={ (): void => setFilter('all')}
                    >All</a>
                    <a
                        className={ filter === 'favourite' ? 'selected': '' }
                        href="#favourite"
                        onClick={ (): void => setFilter('favourite')}
                    >Favourite</a>
                </div>

                <div className={'list'}>
                    { filteredContacts.map(contact => (
                        <EditableContactListItem
                            key={ contact.id }
                            id={ contact.id }
                            name={ contact.name }
                            email={ contact.email }
                            phone={ contact.phone }
                            isFavourite={ contact.isFavourite }
                            onUpdate={ updateContact }
                            onDelete={ deleteContact }
                        />
                    ))}
                </div>
            </div>
        </>
    );
};
