import * as React from 'react';

import { Contact, ContactInterface } from '../../models/contact';


type ContactFormProps = {
    onSubmit: ( contact: Contact ) => void;
};


export const ContactForm: React.FC<ContactFormProps> = ({ onSubmit }) => {
    const [ contact, setContact ] = React.useState<ContactInterface>({ name: '', email: '', phone: '' });

    const handleInputChange = ( event: React.BaseSyntheticEvent ): void => {
        const target = event.target;

        // Update on change.
        setContact({ ...contact , [target.name]: target.value });
    };


    const handleFormSubmit = ( event: React.BaseSyntheticEvent ): void => {
        onSubmit(new Contact(contact.name, contact.email, contact.phone));
        setContact({ name: '', email: '', phone: '' });

        // Prevent normal form submission flow.
        event.preventDefault();
    };

    return (
        <>
            <form onSubmit={ handleFormSubmit } className={'form'}>
                <span> Add New: </span>
                <span className={'field name'}>
                    <input
                        type='text'
                        name='name'
                        placeholder='Name'
                        required
                        value={ contact.name }
                        onChange={ handleInputChange }
                    />
                </span>
                <span className={'field email'}>
                    <input
                        type='email'
                        placeholder='Email'
                        name='email'
                        required
                        value={ contact.email }
                        onChange={ handleInputChange }
                    />
                </span>
                <span className={'field phone'}>
                    <input
                        type='tel'
                        name='phone'
                        pattern='[0-9]{3}-[0-9]{4}-[0-9]{3}'
                        placeholder='Phone: 111-1111-111'
                        value={ contact.phone }
                        onChange={ handleInputChange }
                    />
                </span>
                <span className={'field'}><button className={'button button-primary'} type={'submit'}>Add</button></span>
            </form>
        </>
    );
};
