/* eslint-disable no-unused-vars */

import { uuid } from '../utils'

export class Contact {

    constructor (
        public name: string,
        public email: string,
        public phone: string = '',
        public isFavourite: boolean = false,
        public id: number = uuid()
    ) { }

    public mergeWith(contact: Contact): void{
        this.name = contact.name;
        this.email = contact.email;
        this.phone = contact.phone;
        this.isFavourite = contact.isFavourite;
    }
}


export interface ContactInterface {
    name: string;
    email: string;
    phone?: string;
    isFavourite?: boolean;
    id?: number;
}
