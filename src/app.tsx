import * as React from 'react';

import { ContactList } from './components/contact/contact-list';
import './app.scss'


export const App = ( ): React.ReactElement => (
    <ContactList />
);
