# A Simple Contacts Application
A simple React application that allows you to create, update, delete contacts.
Contacts can be marked favourites and filtered on this criteria.

How To Run?
-----------
After cloning the repo, run `yarn` or `npm install` to install the dependencies.
Then run `yarn start` or `npm run start` to start the development server. 
This will start the development server and open the browser.

To check ESLint report run `yarn lint` or `npm run lint`.
 
